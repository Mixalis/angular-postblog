import {Component, Input, OnInit} from '@angular/core';
import {PostService} from '../services/post.service';

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
    siteTitle = 'Mixalis';

    description: string;
    posts: any[];

    constructor(private postService: PostService) {

    }

    ngOnInit(): void {
        this.description = this.postService.description;
        this.posts = this.postService.posts;
    }
}


