export class Post {
    title: string;
    content: string;
    loveIts: number;
    createdAt: Date;

    constructor(title: string, content: string) {
        this.title = title;
        this.content = content;
        this.createdAt = this.randomDate();
        this.loveIts = 0;
    }

    randomDate() {
        return new Date(+(new Date()) - Math.floor(Math.random() * 100000000000));
    }
}
