import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PostListComponent} from './post-list/post-list.component';
import {PostListItemComponent} from './post-list-item/post-list-item.component';

import {PostService} from './services/post.service';
import {AuthComponent} from './auth/auth.component';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
    {path: 'posts', component: PostListComponent},
    {path: 'auth', component: AuthComponent},
    {path: '', component: PostListComponent}
];


@NgModule({
    declarations: [
        AppComponent,
        PostListComponent,
        PostListItemComponent,
        AuthComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [
        PostService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
