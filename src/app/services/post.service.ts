import {Post} from '../post-list/post.object';

export class PostService {

    description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a justo mauris.' +
        ' Ut purus lacus, dapibus id nibh maximus, tristique porta velit. Donec placerat vel ipsum tempus' +
        ' pharetra. Pellentesque scelerisque non ipsum id euismod. Donec interdum quis turpis vitae ' +
        'ullamcorper. Sed sit amet posuere ligula. Mauris dapibus ultrices odio nec malesuada.';

    posts = [
        new Post('Site Headless, Drupal <-> Angular', this.description),
        new Post('Prochaine étape, GraphQL', this.description),
        new Post('Comprendre le JavaScript', this.description),
        new Post('Le typeScript c\'est la vie', this.description),
        new Post('Premier article', this.description)
    ];

    addVoteAll() {
        for (const post of this.posts) {
            post.loveIts++;
        }
    }

    subVoteAll() {
        for (const post of this.posts) {
            post.loveIts--;
        }
    }

    addVote(index: number) {
        this.posts[index].loveIts++;
    }

    subVote(index: number) {
        this.posts[index].loveIts--;
    }
}
