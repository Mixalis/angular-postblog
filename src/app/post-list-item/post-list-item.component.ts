import {Component, OnInit, Input} from '@angular/core';
import {PostService} from '../services/post.service';
import {Post} from '../post-list/post.object';

@Component({
    selector: 'app-post-list-item',
    templateUrl: './post-list-item.component.html',
    styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

    @Input() post: Post;
    @Input() indexPost: number;

    constructor(private postService: PostService) {
    }

    ngOnInit() {
    }

    itsCool(index: number) {
        this.postService.addVote(index);
    }

    itsBad(index: number) {
        this.postService.subVote(index);
    }

}
